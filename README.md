# eDM Generator - Readme

**This is our eDM generator used to create eDMs from JSON and HTML templates.**

Hopefully this will make the process much much easier and faster.

## Info

eDMs are Electronic Direct Marketing, or, more simply, [Email Marketing](https://en.wikipedia.org/wiki/Email_marketing).

Often, emails consist of ugly, disgusting table-based HTML that are manually copy-pasted from a master template file into specific emails - this takes precious time and is extremely boring.

This project's goal is to be an email generator - one which takes in JSON data and spits out a complete email based on the JSON's selected templates and text.

## Setup

- Install [Middleman](https://middlemanapp.com/) and follow its instructions
- Use **middleman serve** for live viewing/editing of templates
- Use **[middleman build](https://middlemanapp.com/basics/build_and_deploy)** to output rasterized versions of the templates as usable emails

Once a site is running in server mode, you can access it via [127.0.0.1:4567](http://127.0.0.1:4567/).

You may also go to [localhost:4567/__middleman/sitemap](http://localhost:4567/__middleman/sitemap/) to view middleman's automatically-generated [Sitemap](https://middlemanapp.com/advanced/sitemap/).

## Instructions

- Cast magic
- Roll 2D6
- ???
- Profit
