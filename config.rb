###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false



# General configuration
# page "/*", :layout => "layout"  # default layout for all pages
page "/emails/*", :layout => "layout-email"
# activate :directory_indexes



# Generate emails from data
data.emails.each do |filename, filedata|
  proxy "/emails/#{filename}/index.html", "emails/template.html", :locals => {
    :email_data => filedata
  }, :ignore => true
end



###
# Helpers
###

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Build-specific configuration
configure :build do
  # Minify CSS on build
  # activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript
end
